/*
 * Copyright (c) 2018 Yasuo Tabei
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published bytes 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "FM.hpp"

using namespace::std;
using namespace::boost;

double gettimeofday_sec()
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return tv.tv_sec + (double)tv.tv_usec*1e-6;
}

void FM::readfile(istream &is, vector<int> &labels, vector<vector<pair<uint32_t, float> > > &fvs) {
  datDim_ = 0;
  string line;
  while (getline(is, line)) {
    labels.resize(labels.size() + 1);
    fvs.resize(fvs.size() + 1);
    int &label = labels[labels.size() - 1];
    vector<pair<uint32_t, float> > &fv = fvs[fvs.size() - 1];
    
    stringstream ss(line);
    ss >> label;
    
    char sep = 0;
    uint32_t fid;
    float val;
    while (ss >> fid >> sep >> val) {
      fv.push_back(make_pair(fid, val));
      if (datDim_ < fid)
	datDim_ = fid;
    }

    fv.swap(fv);
  }
  datDim_++;
}

void FM::generateRandomMatrix() {
  mt19937               gen(static_cast<unsigned long>(time(0)));
  cauchy_distribution<float> dst(0.f, beta_);
  variate_generator<mt19937&, cauchy_distribution<float> > cauchy(gen, dst);  
  //random_device rd;
  //cauchy_distribution<float> cauchy(0.f, beta_*1.f);
  size_t hashdim2 = hashdim_ >> 1;

  randomMat_.resize(hashdim2);
  for (size_t i = 0; i < hashdim2; ++i) {
    vector<float> &randomVec = randomMat_[i];
    randomVec.resize(datDim_);
    for (size_t j = 0; j < datDim_; ++j) 
      randomVec[j] = cauchy();
  }
}

void FM::computeHashFvs(vector<vector<pair<uint32_t, float> > > &fvs, vector<int> &labels, ostream &os) {
  size_t hashdim2 = hashdim_ >> 1;

  //  hashmat.resize(fvs.size());
  float coef = sqrt(2.f/(float)hashdim_);
  for (size_t i = 0; i < fvs.size(); ++i) {
    //    vector<float> &hashvec = hashmat[i];
    //    hashvec.resize(hashdim_);
    const vector<pair<uint32_t, float> > &fv = fvs[i];
    
    vector<float> hashvec(hashdim_);
    for (size_t j = 0; j < hashdim2; ++j) {
      vector<float> &randomVec = randomMat_[j];
      float sum = 0.f;
      for (size_t k = 0; k < fv.size(); ++k) 
	sum += fv[k].second * randomVec[fv[k].first];

      hashvec[(j << 1)]     = coef * sin(sum);
      hashvec[(j << 1) + 1] = coef * cos(sum);
    }
    
    os << labels[i];
    for (size_t j = 0; j < hashvec.size(); ++j) 
      os << " " << j+1 << ":" << hashvec[j];
    os << endl;
  }
}

void FM::output(ostream &os, vector<vector<float> > hashedfvs, vector<int> &labels) {
  for (size_t i = 0; i < hashedfvs.size(); ++i) {
    vector<float> &hashedfv = hashedfvs[i];
    os << labels[i];
    for (size_t j = 0; j < hashedfv.size(); ++j) 
      os << " " << j + 1 << ":" << hashedfv[j];
    os << endl;
  }
}

void FM::run(const char *input_file, const char *output_file, uint64_t hashdim, float beta) {
  hashdim_ = hashdim;
  beta_    = beta;

  vector<int> labels;
  vector<vector<pair<uint32_t, float> > > fvs;
  {
    ifstream ifs(input_file);
    if (!ifs) {
      cerr << "cannot open: " << input_file << endl;
      exit(1);
    }
    readfile(ifs, labels, fvs);
  }
  
  double stime = gettimeofday_sec();
  generateRandomMatrix();

  ofstream ofs(output_file);
  if (!ofs) {
    cerr << "cannot open: " << output_file << endl;
    exit(1);
  }
  vector<vector<float> > hashfvs;
  computeHashFvs(fvs, labels, ofs);
  double etime = gettimeofday_sec();

  cout << "CPU time (sec): " << (etime - stime) << endl;

  double mem = 0.0;
  for (size_t i = 0; i < fvs.size(); ++i) {
    mem += fvs[i].size() * (sizeof(uint32_t) +  sizeof(float));
  }
  cout << "input size(bytes): " << mem << endl;

  mem = 0.0;
  for (size_t i = 0; i < hashfvs.size(); ++i) {
    mem += hashfvs[i].size() * sizeof(float);
  }
  cout << "hash size(bytes): " << mem << endl;

  cout << "random matrix size: " << datDim_ * hashdim_ * sizeof(float) << endl;
  /*  
  {
    ofstream ofs(output_file);
    if (!ofs) {
      cerr << "cannot open: " << output_file << endl;
      exit(1);
    }
    output(ofs, hashfvs, labels);
    ofs.close();
  }
  */
}

