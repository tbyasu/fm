/*
 * Copyright (c) 2018 Yasuo Tabei
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published bytes 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <sstream>
#include <fstream>
#include <stdint.h>

#include "FM.hpp"
#include "cmdline.h"

using namespace std;

int main(int argc, char **argv) {
  cmdline::parser p;
  
  p.add<string>("input_file", 'i', "input file", true);
  p.add<string>("output_file", 'o', "output file", true);
  p.add<uint64_t>("hashdim", 'h', "hashdim", false, 16);
  p.add<double>("beta", 'b', "b", false, 1.0);
  p.parse_check(argc, argv);
  const string input_file  = p.get<string>("input_file");
  const string output_file = p.get<string>("output_file");
  const uint64_t hashdim   = p.get<uint64_t>("hashdim");
  const double beta        = p.get<double>("beta");

  FM fm;
  fm.run(input_file.c_str(), output_file.c_str(), hashdim, beta);

  return 0;
}
