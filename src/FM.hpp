/*
 * Copyright (c) 2018 Yasuo Tabei
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published bytes 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once 

#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <sstream>
#include <fstream>
#include <stdint.h>
#include <cmath>
#include <ctime>
#include <sys/time.h>
//#include <random>
//#include "./boost/random.hpp"
//#include "./boost/math/distributions/cauchy.hpp"
#include <boost/random.hpp>
#include <boost/random/variate_generator.hpp>

class FM {
private:
  void readfile(std::istream &is, std::vector<int> &labels, std::vector<std::vector<std::pair<uint32_t, float> > > &fvs);
  void generateRandomMatrix();
  void computeHashFvs(std::vector<std::vector<std::pair<uint32_t, float> > > &fvs, std::vector<int> &labels, std::ostream &os);
  void output(std::ostream &os, std::vector<std::vector<float> > hashedfvs, std::vector<int> &labels);
public:
  void run(const char *input_file, const char *output_file, uint64_t hashdim, float beta);
private:
  uint64_t datDim_;
  uint64_t hashdim_;
  float beta_;

  std::vector<std::vector<float> > randomMat_;
};
